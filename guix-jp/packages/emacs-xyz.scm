;;; Copyright © 2023 gemmaro <gemmaro.dev@gmail.com>
;;; Copyright © 2025 p-snow <public@p-snow.org>
;;;
;;; This file is part of the Guix-jp channel.
;;;
;;; The Guix-jp channel is free software: you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (guix-jp packages emacs-xyz)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix build-system emacs)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix-jp packages migemo))

(define-public emacs-ddskk-posframe
  (let ((commit "299493dd951e5a0b43b8213321e3dc0bac10f762")
        (revision "0"))
    (package
      (name "emacs-ddskk-posframe")
      (version (git-version "1.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/conao3/ddskk-posframe.el")
               (commit commit)))
         (sha256
          (base32 "1rsy0wjncxzjhis8jrxpxjh9l9pw0bngg1sb4dj5qvhsgszhawpn"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:tests? #t
        #:test-command #~(list "buttercup" "-L" ".")))
      (native-inputs (list emacs-buttercup))
      (propagated-inputs (list emacs-posframe emacs-ddskk))
      (home-page "https://github.com/conao3/ddskk-posframe.el")
      (synopsis "Show Henkan tooltip for ddskk via posframe")
      (description
       "@samp{ddskk-posframe.el} provide Henkan tooltip for ddskk via
posframe, which can pop up a frame at point.  Use
@code{ddskk-posframe-mode} to enable this minor mode.")
      (license license:agpl3))))

(define-public emacs-japanese-holidays
  (let ((revision "0")
        (commit "324b6bf2f55ec050bef49e001caedaabaf4fa35d"))
    (package
      (name "emacs-japanese-holidays")
      (version (git-version "1.190317" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/emacs-jp/japanese-holidays")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1bxbxczsf0b7jiim2anjh16h243khyrcv4k07qg0yy8c9zrl18aq"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:tests? #t
        #:test-command #~(list "make" "test")))
      (home-page "https://github.com/emacs-jp/japanese-holidays")
      (synopsis "Japanese holidays for Emacs calendar")
      (description
       "This package defines Japanese holidays for Emacs calendar and provides
highlighting capabilities for holidays and weekends.")
      (license license:gpl2+))))

(define-public emacs-migemo
  (let ((revision "0")
        (commit "7d78901773da3b503e5c0d5fa14a53ad6060c97f"))
    (package
      (name "emacs-migemo")
      (version (git-version "1.9.2" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/emacs-jp/migemo")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1098lf50fcm25wb41g1b27wg8hc3g2w6cgjq02sc8pq6qnrr0ql2"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'patch-migemo-directory
              (lambda* (#:key inputs #:allow-other-keys)
                (emacs-substitute-variables "migemo.el"
                  ("migemo-directory"
                   (search-input-directory inputs "share/migemo/utf-8"))
                  ("migemo-command"
                   (search-input-file inputs "bin/cmigemo"))))))))
      (inputs (list cmigemo migemo-dict))
      (home-page "http://0xcc.net/migemo/")
      (synopsis "Japanese incremental search in Emacs")
      (description
       "@samp{emacs-migemo} enables incremental search of Japanese text using Romaji in
Emacs.  It serves as an Emacs plugin for @samp{migemo}, which is a backend
program that allows various editors to offer this functionality.")
      (license license:gpl2+))))
