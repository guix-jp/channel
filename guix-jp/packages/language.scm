;;; Copyright © 2023 gemmaro <gemmaro.dev@gmail.com>
;;;
;;; This file is part of the Guix-jp channel.
;;;
;;; The Guix-jp channel is free software: you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (guix-jp packages language)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages base))

(define-public darts
  (package
    (name "darts")
    (version "0.32")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://chasen.org/~taku/software/darts/src/darts-"
             version ".tar.gz"))
       (sha256
        (base32 "11cj4sgani8ylah1pczzx0fkzyabwml9716g5awr6pd0y210pz0d"))))
    (build-system gnu-build-system)
    (home-page "http://chasen.org/~taku/software/darts/")
    (synopsis "Simple C++ template library for building double-arrays")
    (description
     "Darts is a simple C++ template library for building a Double-Array
@url{https://doi.org/10.1109/32.31365, [Aoe 1989]}, a data structure
to represent a Trie.  It works faster than other Trie implementations,
such as hash trees, digital tries, patrician trees, and pseudo-Trie
with Suffix Arrays.  The original Double-Array was a framework that
allowed dynamic addition and deletion of keys, but Darts focused on
converting sorted dictionaries into a Double-Array in bulk.")
    (license (list license:lgpl2.1 license:bsd-3))))

(define-public chasen
  (package
    (name "chasen")
    (version "2.4.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://ja.osdn.net/frs/redir.php?"
                           "m=nchc&f=chasen-legacy%2F56305%2Fchasen-" version
                           ".tar.xz"))
       (file-name (string-append name "-" version ".tar.xz"))
       (sha256
        (base32 "1y1k6vxrz469a0rwv6xxsi2v6ydlsmdmn0v4mjy0z1i6j41k9486"))))
    (build-system gnu-build-system)
    (inputs (list libiconv darts))
    (home-page "https://chasen-legacy.osdn.jp/")
    (synopsis "Morphological parser for the Japanese language")
    (description
     "ChaSen is a morphological parser for the Japanese language.
This tool for analyzing morphemes was developed at the Matsumoto
laboratory, Nara Institute of Science and Technology.")
    (license license:bsd-3)))
