日本語/[English](README.en.md)

# Guix-jp公式チャンネル
Guix-jpが提供する[Guix](https://guix.gnu.org/)のチャンネルです。主に日本語関連のパッケージを提供しています。
Guix-jpについては[こちら](http://guix-jp.gitlab.io/)をご覧ください。

# 使ってみる
Guixがインストールされた状態で、`~/.config/guix/channels.scm`を以下のように編集しましょう。
```scheme
(cons* (channel
        (name 'guix-jp)
        (url "https://gitlab.com/guix-jp/channel")
        (branch "main"))
       ;; 他のチャンネルを利用したい場合はここに書くことができます
       ;; (channel
       ;;  (name 'another-channel)
       ;;  (url "https://example.com/another/channel"))
       %default-channels)
```

その後、以下のコマンドを実行します。
```shell
guix pull
```

これで、このチャンネルに登録された全てのパッケージを利用することができます。
インストールしたいパッケージを`guix install`でインストールしましょう。

# 貢献
どんな貢献も歓迎します。質問や改善案、追加パッケージの要望などがあれば、
イシューや[Guix-jpコミュニティ](http://guix-jp.gitlab.io/)で是非お気軽にお声かけください。
勿論マージリクエストも歓迎します。

# ライセンス
このリポジトリは、[GNU General Public License v3.0](LICENSE)の元で公開されています。
